// Singleton Design Pattren 

package ecommerce.Models;

public class connectionDB {

    private static connectionDB connection = null;

    private connectionDB() {
    }

    public static connectionDB getConnection() {
        if (connection == null) {
            connection = new connectionDB();
        }
        return connection;

    }

    public static class getConnection extends connectionDB {

        public getConnection() {
        }
    }
}


